#include "def.h"
#include "msg.h"

#include <starlet.h>

s32 vec$init(vec v)
{
	v[0] = 0;
	return RE$_OK;
}

s32 vec$add(vec v, i32 code)
{
	if (v[0] - 2 > VEC$MAX_CAP - 1) {
		v[v[0] + 1] = RE$_TOOMANY;
		v[v[0] + 2] = 0;

		return RE$_TOOMANY;
	}

	v[v[0] + 1] = code;
	v[v[0] + 2] = 0;
	
	v[0] += 2;
	return RE$_OK;
}

s32 vec$sig(vec v)
{
	if (v[0] == 0) {
		v[0] = 2;
		v[1] = RE$_OK;
		v[2] = 0;
	}

	i32 sys_res;
	sys_res = sys$putmsg(v, NULL, NULL, 0);
	if (!(sys_res & 1))
		return RE$_SYSPUTMSG;

	return RE$_OK;
}
