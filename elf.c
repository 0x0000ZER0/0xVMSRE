#include "def.h"

#include <stdio.h>

void elf$hdr_print(elf$hdr *ehdr)
{
	printf("IDENT:   [%02X%02X%02X%02X %02X%02X%02X%02X"	
		" %02X%02X%02X%02X %02X%02X%02X%02X]\n",
		ehdr->elf$ident[0x0], ehdr->elf$ident[0x1],
		ehdr->elf$ident[0x2], ehdr->elf$ident[0x3],
		ehdr->elf$ident[0x4], ehdr->elf$ident[0x5],
		ehdr->elf$ident[0x6], ehdr->elf$ident[0x7],
		ehdr->elf$ident[0x8], ehdr->elf$ident[0x9],
		ehdr->elf$ident[0xA], ehdr->elf$ident[0xB],
		ehdr->elf$ident[0xC], ehdr->elf$ident[0xD],
		ehdr->elf$ident[0xE], ehdr->elf$ident[0xF]);
	printf("-> MAG0:    [0x%02X]\n", ehdr->elf$ident[EI$MAG0]);
	printf("-> MAG1:    [%c]\n", ehdr->elf$ident[EI$MAG1]);
	printf("-> MAG2:    [%c]\n", ehdr->elf$ident[EI$MAG2]);
	printf("-> MAG3:    [%c]\n", ehdr->elf$ident[EI$MAG3]);

	static const char *class_desc[] = {
		"it contains 32 bit objects",
		"it contains 64 bit objects"
	};
	elf64_byte class;
	class = ehdr->elf$ident[EI$CLASS];
	printf("-> CLASS:   [%u], '%s'\n", class, class_desc[class - 1]);

	static const char *data_desc[] = {
		"object file data structures are little-endian",
		"object file data structures are big-endian",
	};
	elf64_byte data;
	data = ehdr->elf$ident[EI$DATA];
	printf("-> DATA:    [%u], '%s'\n", data, data_desc[data - 1]);
	printf("-> VERSION: [%u]\n", ehdr->elf$ident[EI$VERSION]);

	static const char *osabi_desc[] = {
		"System V ABI",
		"HP-UX OS",
		"OpenVMS",
		"Unknown"
	};
	elf64_byte osabi;
	osabi = ehdr->elf$ident[EI$OSABI] > ELFOSABI$HPUX ? 3 : ehdr->elf$ident[EI$OSABI];
	osabi = ehdr->elf$ident[EI$OSABI] == ELFOSABI$OPENVMS ? 2 : 3;
	printf("-> OS ABI:  [%u], '%s'\n", ehdr->elf$ident[EI$OSABI], osabi_desc[osabi]);	
	printf("-> ABI VER: [%u]\n", ehdr->elf$ident[EI$ABIVERSION]);	

	printf("-> PADDING\n");

	static const char *e_type_desc[] = {
		"No file type",
		"Relocatable object file",
		"Executable file",
		"Shared object file",
		"Core file",
		"Enviornment-specific use",
		"Processor-specific use"
	};
	elf64_half e_type;
	e_type = ehdr->elf$type;
	if (e_type >= ET$LOOS && e_type <= ET$HIOS)
		e_type = 5;
	else if (e_type >= ET$LOPROC)
		e_type = 6;
	printf("TYPE:    [%u], '%s'\n", ehdr->elf$type, e_type_desc[e_type]);

		static const char *e_machine_desc[] = {
		"None",
		"AT&T WE 32100",
		"SPARC",
		"Intel Architecture",
		"Motorola 68000",
		"Motorola 88000",
		"Intel 80486",
		"Intel 80860",
		"MIPS RS3000 Big-Endian",
		"MIPS RS4000 Big-Endian",
		"HPPA",
		"Sun's v8plus",
		"PowerPC",
		"PowerPC64",
		"Cell BE SPU",
		"SuperH",
		"SPARC v9 64-bit",
		"HP/Intel IA-64",
		"AMD x86-64",
		"IBM S/390",
		"Axis Communications 32-bit embedded processor",
		"NEC v850",
		"Renesas M32R",
		"Renesas H8/300,300H,H8S",
		"Panasonic/MEI MN10300, AM33",
		"ADI Blackfin Processor",
		"Fujitsu FR-V",
		"Atmel AVR32"
	};

	elf64_half e_machine;
	e_machine = ehdr->elf$machine;
	switch (e_machine) {
	case 10:
		e_machine = 9;
		break;
	case 15:
		e_machine = 10;
		break;
	case 18:
		e_machine = 11;
		break;
	case 20:
		e_machine = 12;
		break;
	case 21:
		e_machine = 13;
		break;
	case 23:
		e_machine = 14;
		break;
	case 42:
		e_machine = 15;
		break;
	case 43:
		e_machine = 16;
		break;
	case 50:
		e_machine = 17;
		break;
	case 62:
		e_machine = 18;
		break;
	case 22:
		e_machine = 19;
		break;
	case 76:
		e_machine = 20;
		break;
	case 87:
		e_machine = 21;
		break;
	case 88:
		e_machine = 22;
		break;
	case 46:
		e_machine = 23;
		break;
	case 89:
		e_machine = 24;
		break;
	case 106:
		e_machine = 25;
		break;
	case 0x5441:
		e_machine = 26;
		break;
	case 0x18AD:
		e_machine = 27;
		break;
	}

	printf("MACHINE: [%u], '%s'\n", ehdr->elf$machine, e_machine_desc[e_machine]);
	printf("VERSION: [%u]\n", ehdr->elf$version);
	printf("ENTRY:   [0x%016llX]\n", ehdr->elf$entry);
	printf("FLAGS:   [0x%08X]\n", ehdr->elf$flags);
	printf("ELF HEADER SIZE:      [%u]\n", ehdr->elf$ehsize);
	printf("PROGRAM HEADER SIZE:  [%u]\n", ehdr->elf$phentsize);
	printf("SECTION HEADER SIZE:  [%u]\n", ehdr->elf$shentsize);
	printf("PROGRAM HEADER COUNT: [%u]\n", ehdr->elf$phnum);
	printf("SECTION HEADER COUNT: [%u]\n", ehdr->elf$shnum);
	printf("PROGRAM HEADERS START OFFSET:   [%llu]\n", ehdr->elf$phoff);
	printf("SECTION HEADERS START OFFSET:   [%llu]\n", ehdr->elf$shoff);
	printf("INDEX OF STRING SECTION HEADER: [%u]\n", ehdr->elf$shstrndx);
}

void elf$shdr_print(elf$shdr *shdr, u8 *strbuf)
{
	printf("NAME:  %s\n", shdr->sh$name + strbuf);

	static const char *type_desc[] = {
		"NULL",
		"PROGBITS",
		"SYMTAB",
		"STRTAB",
		"RELA",
		"HASH",
		"DYNAMIC",
		"NOTE",
		"NOBITS",
		"REL",
		"SHLIB",
		"DYNSYM",
		"UNKNOWN",
		"[OS]",
		"[PROC]"
	};

	elf64_word type;
	type = shdr->sh$type;
	if (type >= SHT$HOOS && type <= SHT$LOOS)
		type = 13;
	else if (type >= SHT$HOPROC && type <= SHT$LOPROC)
		type = 14;
	else if (type > SHT$DYNSYM) 
		type = 12;
	
	printf("TYPE:  %s\n", type_desc[shdr->sh$type]);

	elf64_xword flag;
	flag = shdr->sh$flags;
	char flag_w;
	flag_w = flag & SHF$WRITE ? 'W' : '-';
	char flag_a;
	flag_a = flag & SHF$ALLOC ? 'A' : '-';
	char flag_x;
	flag_x = flag & SHF$EXECINSTR ? 'X' : '-';
	char flag_o;
	flag_o = flag & SHF$MASKOS ? 'O' : '-';
	char flag_p;
	flag_p = flag & SHF$MASKPROC ? 'P' : '-';
	printf("FLAG:  %c%c%c%c%c\n", flag_w, flag_a, flag_x, flag_o, flag_p);

	printf("ADDR:  0x%016llX\n", shdr->sh$addr);
	printf("OFF:   0x%016llX\n", shdr->sh$offset);
	printf("SIZE:  0x%016llX\n", shdr->sh$size);

	elf64_word link;
	link = shdr->sh$link;
	if (link > SHT$DYNSYM)
		link = 12;
	printf("LINK:  %s\n", type_desc[link]);

	elf64_word info;
	info = shdr->sh$info;
	if (info > SHT$DYNSYM)
		info = 12;
	printf("INFO:  %s\n", type_desc[info]);

	printf("ALIGN: %03llu\n", shdr->sh$addralign);
	printf("ENT:   %010llu\n", shdr->sh$entsize);
}
