#include "def.h"
#include "msg.h"

#include <rms.h>
#include <starlet.h>
#include <ssiodef.h>
#include <iodef.h>
#include <stdio.h>
#include <secdef.h>
#include <descrip.h>

s32 file$init(vec v, file *ctx, i8 *path, u8 len)
{
	ctx->opt$map = TRUE;

	ctx->bin$fab = cc$rms_fab;

	ctx->bin$fab.fab$l_fna = path;
	ctx->bin$fab.fab$b_fns = len;
	ctx->bin$fab.fab$b_fac = FAB$M_GET | FAB$M_PUT;
	ctx->bin$fab.fab$l_fop = FAB$M_UFO;

	s32 sys_res;
	sys_res = sys$open(&ctx->bin$fab);
	if (!(sys_res & 1)) {
		vec$add(v, sys_res);
		vec$add(v, RE$_SYSOPEN);
		return RE$_SYSOPEN;
	}

	return RE$_OK;
}

s32 file$map(vec v, file *ctx, u8 **addr)
{
	s32 sys_res;

	$DESCRIPTOR(sec_name, "RE_LD_EXE_SEC");

	va_range in_addr;
	sys_res = sys$crmpsc(&in_addr, &ctx->ret$addr, 0, SEC$M_EXPREG, &sec_name, 0,
			0, ctx->bin$fab.fab$l_stv, 0, 0, 0x00FF, 0);
	if (!(sys_res & 1)) {
		vec$add(v, sys_res);
		vec$add(v, RE$_SYSCRMPSC);
		return RE$_SYSCRMPSC;
	}

	*addr = ctx->ret$addr.va_range$ps_start_va;

	return RE$_OK;

}

s32 file$unmap(vec v, file *ctx)
{
	s32 sys_res;

	va_range del_addr;
	sys_res = sys$deltva(&ctx->ret$addr, &del_addr, 0);
	if (!(sys_res & 1)) {
		vec$add(v, sys_res);
		vec$add(v, RE$_SYSDELTVA);
		return RE$_SYSDELTVA;
	}

	return RE$_OK;

}

s32 file$read(vec v, file *ctx, u8 *buf, u64 len, u64 off)
{
	if (off == 0)
		off = 512;

	s32 sys_res;
	sys_res = sys$qiow(0, ctx->bin$fab.fab$l_stv, IO$_READVBLK, 
		    ctx->bin$iosb, NULL, 0, buf, len, off / 512, 0, 0, 0);
	if (!(sys_res & 1)) {
		vec$add(v, sys_res);
		vec$add(v, RE$_SYSREAD);
		return RE$_SYSREAD;
	}

	vec$add(v, ctx->bin$iosb[0]);
	return RE$_OK;
}

s32 file$free(vec v, file *ctx)
{
	s32 sys_res;

	if (ctx->opt$map) {
		sys_res = sys$dassgn(ctx->bin$fab.fab$l_stv);
		if (!(sys_res & 1)) {
			vec$add(v, sys_res);
			vec$add(v, RE$_SYSDASSGN);
			return RE$_SYSDASSGN;
		}

		return RE$_OK;
	}	

	sys_res = sys$qiow(0, ctx->bin$fab.fab$l_stv, IO$_DEACCESS, 
		    ctx->bin$iosb, NULL, 0, NULL, 0, 0, 0, 0, 0);
	if (!(sys_res & 1)) {
		vec$add(v, sys_res);
		vec$add(v, RE$_SYSCLOSE);
		return RE$_SYSCLOSE;
	}

	return RE$_OK;
}
