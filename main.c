#include "def.h"
#include "msg.h"

#include <stdio.h>
#include <string.h>
#include <lib$routines.h>
#include <cli$routines.h>
#include <ots$routines.h>
#include <decc$types.h>
#include <climsgdef.h>
#include <rmsdef.h>
#include <namdef.h>

extern void* vmsre_module;

i8	glb$bin_path[NAM$C_MAXRSS];
u16 	glb$bin_path_len = 0;
u8  	glb$bin_set = FALSE;
u8  	*glb$bin_ptr;
file	glb$file;
vec	glb$vec;

int set_binary(void)
{
	s32 cli_res;

	$DESCRIPTOR(path, "PATH");
	cli_res = cli$present(&path);
	if (cli_res != CLI$_PRESENT)
		return RE$_MISSQUAL;

	$DESCRIPTOR(value, glb$bin_path);
	value.dsc$w_length = sizeof (glb$bin_path);

	cli_res = cli$get_value(&path, &value, &glb$bin_path_len);
	if (!(cli_res & 1))
		return RE$_GETVAL;

	s32 file_res;

	vec$init(glb$vec);
	file_res = file$init(glb$vec, &glb$file, glb$bin_path, glb$bin_path_len);
	if (!(file_res & 1)) 
	{
		cli_res = file_res;
		goto _EXIT_;
	}

	vec$init(glb$vec);
	file_res = file$map(glb$vec, &glb$file, &glb$bin_ptr);
	if (!(file_res & 1)) 
	{
		cli_res = file_res;
		goto _FILE_FREE_;
	}

	glb$bin_set = TRUE;

	return RE$_OK;

_FILE_FREE_:
	file_res = file$free(glb$vec, &glb$file);
	if (!(file_res & 1)) 
		cli_res = file_res;
_EXIT_:
	return cli_res;
}

int show_binary(void)
{
	if (!glb$bin_set)
		return RE$_NOBIN;

	printf("%.*s (LENGTH:%u)\n", 
		(int)glb$bin_path_len, glb$bin_path, glb$bin_path_len);
	return RE$_OK;
}

int show_header(void)
{
	s32 cli_res;
	cli_res = RE$_OK;

	vec$init(glb$vec);

	if (!glb$bin_set)
	{
		cli_res = RE$_NOBIN;
		vec$add(glb$vec, cli_res);
		goto _EXIT_;
	}

	elf$hdr *ehdr;
	ehdr = (elf$hdr*)glb$bin_ptr;

	elf$hdr_print(ehdr);

_EXIT_:
	return cli_res;
}

int show_section(void)
{
	s32 cli_res;
	cli_res = RE$_OK;

	vec$init(glb$vec);

	if (!glb$bin_set)
	{
		cli_res = RE$_NOBIN;
		vec$add(glb$vec, cli_res);
		goto _EXIT_;
	}

	elf$hdr *ehdr;
	ehdr = (elf$hdr*)glb$bin_ptr;

	elf$shdr *shdrs;
	shdrs = (elf$shdr*)(glb$bin_ptr + ehdr->elf$shoff);

	elf$shdr *strshdr;
	strshdr = shdrs + ehdr->elf$shstrndx;

	u8 *strbuf;
	strbuf = glb$bin_ptr + strshdr->sh$offset;

	$DESCRIPTOR(dump, "DUMP");
	s32 dump_res;
	dump_res = cli$present(&dump);

	elf$shdr *curr;

	$DESCRIPTOR(index, "INDEX");
	cli_res = cli$present(&index);
	if (cli_res != CLI$_PRESENT) {
		for (elf64_half i = 0; i < ehdr->elf$shnum; ++i) {
			curr = shdrs + i;

			printf("----------------- [%u] -----------------\n", i);
			elf$shdr_print(curr, strbuf);

			if (dump_res == CLI$_PRESENT) {
				printf("------- DUMP -------\n");
				dump$blob(glb$bin_ptr + curr->sh$offset, curr->sh$size); 
			}
		}

		cli_res = RE$_OK;
	} 
	else {
		i8 val_buf[8];

		desc_s val_desc;
		val_desc.dsc$a_pointer = val_buf;
		val_desc.dsc$w_length  = sizeof (val_buf);
		val_desc.dsc$b_class   = DSC$K_CLASS_S;
		val_desc.dsc$b_dtype   = DSC$K_DTYPE_T;

		u16 ret_len;
		cli_res = cli$get_value(&index, &val_desc, &ret_len);
		if (!(cli_res & 1)) {
			cli_res = RE$_GETVAL;
			vec$add(glb$vec, cli_res);
			goto _EXIT_;
		}

		val_desc.dsc$w_length = ret_len;

		elf64_half index;
		ots$cvt_tu_l(&val_desc, &index);	
		if (index >= ehdr->elf$shnum) {
			cli_res = RE$_OUTOFRNG;
			vec$add(glb$vec, cli_res);
			goto _EXIT_;
		}

		curr = shdrs + index;

		printf("----------------- [%u] -----------------\n", index);
		elf$shdr_print(curr, strbuf);

		if (dump_res == CLI$_PRESENT) {
			printf("------- DUMP -------\n");
			dump$blob(glb$bin_ptr + curr->sh$offset, curr->sh$size); 
		}
	}


_EXIT_:
	return cli_res;
}

int show_library(void)
{
	s32 cli_res;
	cli_res = RE$_OK;

	vec$init(glb$vec);

	if (!glb$bin_set)
	{
		cli_res = RE$_NOBIN;
		vec$add(glb$vec, cli_res);
		goto _EXIT_;
	}

	elf$hdr *ehdr;
	ehdr = (elf$hdr*)glb$bin_ptr;

	elf$shdr *shdrs;
	shdrs = (elf$shdr*)(glb$bin_ptr + ehdr->elf$shoff);

	elf$shdr *strshdr;
	strshdr = shdrs + ehdr->elf$shstrndx;

	u8 *strbuf;
	strbuf = glb$bin_ptr + strshdr->sh$offset;
	
	u8 found;
	found = FALSE;

	static const i8 dynstr[] = ".dynstr";

	elf$shdr *curr;
	for (elf64_half i = 0; i < ehdr->elf$shnum; ++i) {
		curr = shdrs + i;

		if (memcmp(strbuf + curr->sh$name, dynstr, sizeof (dynstr) - 1) == 0) {
			found = TRUE;
			break;
		}
	}

	if (!found) {
		cli_res = RE$_NOTFND;
		vec$add(glb$vec, cli_res);
		goto _EXIT_;
	}

	u8 *dynbuf;
	dynbuf = glb$bin_ptr + curr->sh$offset;

	desc_s name_desc;
	name_desc.dsc$b_class   = DSC$K_CLASS_S;
	name_desc.dsc$b_dtype   = DSC$K_DTYPE_T;

	i8 file_buf[sizeof (u16) + NAM$C_MAXRSS + 1];

	desc_vs file_desc;
	file_desc.dsc$b_class   = DSC$K_CLASS_VS;
	file_desc.dsc$b_dtype   = DSC$K_DTYPE_VT;
	file_desc.dsc$a_pointer = file_buf;

	static const i8 exe_ext[] = ".EXE";

	$DESCRIPTOR(syslib, "SYS$LIBRARY:");

	i8 lib_name[256];
	u64 lib_name_len;
	for (elf64_xword i = 1, j = 1, c = 1; i < curr->sh$size; ++i) {
		if (dynbuf[i] != '\0')
			continue;

		if (i - j <= 1)
			continue;

		memcpy(lib_name, dynbuf + j, i - j);
		lib_name_len = i - j;

		memcpy(lib_name + lib_name_len, exe_ext, sizeof (exe_ext) - 1);
		lib_name_len += sizeof (exe_ext) - 1;

		name_desc.dsc$a_pointer	= lib_name;
		name_desc.dsc$w_length  = lib_name_len;

		file_desc.dsc$w_maxstrlen = NAM$C_MAXRSS;

		s32 lib_res;
		lib_res = RMS$_NORMAL;

		u32 ctx;
		ctx = 0;
		
		u32 sts;
		sts = 0;

		lib_res = lib$find_file(&name_desc, &file_desc, &ctx, &syslib, NULL, &sts, 0);
		if (lib_res == RMS$_NORMAL) {
			printf("LIBRARY %02llu: %.*s [%llu], PATH: %.*s\n", 
				c, (i32)lib_name_len, lib_name, lib_name_len, syslib.dsc$w_length - 1, syslib.dsc$a_pointer);
		}
		else {
			printf("LIBRARY %02llu: %.*s [%llu], PATH: Not Found\n", 
				c, (i32)lib_name_len, lib_name, lib_name_len);
		}

		lib_res = lib$find_file_end(&ctx);


		j = i + 1;
		++c;
	}
	
_EXIT_:
	return cli_res;
}


int quit(void)
{
	s32 cli_res;
	cli_res = RE$_EXIT;

	if (!glb$bin_set)
		return cli_res;

	s32 file_res;
	file_res = file$unmap(glb$vec, &glb$file);
	if (!(file_res & 1))
		cli_res = file_res;

	file_res = file$free(glb$vec, &glb$file);
	if (!(file_res & 1))
		cli_res = file_res;

	return cli_res;
}

int main(void)
{
	$DESCRIPTOR(prompt, "RE> ");

	s32 cli_res;
	s32 cli_ret;

	do 
	{
		cli_res = cli$dcl_parse(NULL, &vmsre_module, 
				NULL, lib$get_input, &prompt);

		switch (cli_res)
		{
		case CLI$_NORMAL:
			cli_ret = cli$dispatch();
			if (!(cli_ret & 1))
				vec$sig(glb$vec);

			if (cli_ret == RE$_EXIT)
				cli_res = RMS$_EOF;
			break;
		case CLI$_IVVERB:
			break;
		}

	} while (cli_res != RMS$_EOF);

	return RE$_OK;
}
