#ifndef _DEF_H_
#define _DEF_H_

#include <fabdef.h>
#include <rabdef.h>
#include <va_rangedef.h>
#include <descrip.h>
 
#define NULL  0
#define TRUE  1
#define FALSE 0

typedef char 			i8;
typedef short			i16;
typedef int 			i32;
typedef long long 		i64;
typedef signed char 		s8;
typedef signed short 		s16;
typedef signed int 		s32;
typedef signed long long 	s64;
typedef unsigned char 		u8;
typedef unsigned short 		u16;
typedef unsigned int 		u32;
typedef unsigned long long 	u64;
typedef const char* 		cstr;
typedef char* 			str;

typedef struct dsc$descriptor_d  desc_d;
typedef struct dsc$descriptor_s  desc_s;
typedef struct dsc$descriptor_vs desc_vs;

void dump$blob(u8*, u64);

#define VEC$MAX_CAP 16
typedef i32 vec[VEC$MAX_CAP];

s32 vec$init(vec);
s32 vec$add(vec, i32);
s32 vec$sig(vec);

typedef struct FAB 		fab;
typedef struct RAB 		rab;
typedef struct RAB64 		rab64;
typedef struct RAB64 		rab64;

typedef struct 
{
	u8 opt$map;
	
	va_range ret$addr;

	fab bin$fab;	
	i16 bin$iosb[4];
} file;

s32 file$init(vec, file*, i8*, u8);
s32 file$map(vec, file*, u8**);
s32 file$unmap(vec, file*);
s32 file$read(vec, file*, u8*, u64, u64);
s32 file$free(vec, file*);

typedef u64 elf64_addr;
typedef u64 elf64_off;
typedef u16 elf64_half;
typedef u32 elf64_word;
typedef s32 elf64_sword;
typedef u64 elf64_xword;
typedef s64 elf64_sxword;
typedef u8  elf64_byte;

enum 
{
	EI$MAG0 	= 0,  // file identification
	EI$MAG1 	= 1,  // file identification
	EI$MAG2 	= 2,  // file identification
	EI$MAG3 	= 3,  // file identification
	EI$CLASS 	= 4,  // file class
	EI$DATA 	= 5,  // data encoding
	EI$VERSION 	= 6,  // file version
	EI$OSABI 	= 7,  // OS/ABI identification
	EI$ABIVERSION 	= 8,  // ABI version
	EI$PAD 		= 9,  // start of padding
	EI$NIDENT 	= 16, // size of e_ident[]
};

enum 
{
	ELFOSABI$SYSV 		= 0,  // System V ABI
	ELFOSABI$HPUX 		= 1,  // HP-UX operating System
	ELFOSABI$OPENVMS 	= 13, // OpenVMS
	ELFOSABI$STANDALONE 	= 255 // standalone (embedded) application
};

enum 
{
	ET$NONE 	= 0, 	  // no file type
	ET$REL 		= 1, 	  // relocatable file
	ET$EXEC 	= 2, 	  // executable file
	ET$DYN 		= 3, 	  // shared object file
	ET$CORE 	= 4, 	  // core file
	ET$LOOS 	= 0xFE00, // environment-specific
	ET$HIOS 	= 0xFEFF, // environment-specific
	ET$LOPROC 	= 0xFF00, // processor-specific
	ET$HIPROC 	= 0xFFFF  // processor-specific
};

enum 
{
	SHT$NULL 	= 0, 	// unused section header
	SHT$PROGBITS 	= 1, 	// contains information defined by the program
	SHT$SYMTAB 	= 2, 	// contains a linker symbol table
	SHT$STRTAB 	= 3, 	// contains a string table
	SHT$RELA 	= 4, 	// contains "Rela" type relocation entries
	SHT$HASH 	= 5, 	// contains a symbol hash table
	SHT$DYNAMIC 	= 6, 	// contains dynamic linking tables
	SHT$NOTE 	= 7, 	// contains note information
	SHT$NOBITS 	= 8, 	// contains uninitialized space, 
				// does not occupy any space in the file
	SHT$REL 	= 9, 	// contains "Rel" type relocation entries
	SHT$SHLIB 	= 10, 	// reserved 
	SHT$DYNSYM 	= 11, 	// contains a dynamic loader symbol table

	SHT$LOOS 	= 0x60000000, // environment-specific use
	SHT$HOOS 	= 0x6FFFFFFF, // environment-specific use
	SHT$LOPROC 	= 0x70000000, // processor-specific use
	SHT$HOPROC 	= 0x7FFFFFFF, // processor-specific use
};

enum 
{
	SHF$WRITE = 0x1, 	// section contains writable data
	SHF$ALLOC = 0x2, 	// section contains allocated memory
				// image of program
	SHF$EXECINSTR = 0x4, 	// section contains executable instructions
	
	SHF$MASKOS 	= 0x0F000000, // environment-specific use
	SHF$MASKPROC 	= 0xF0000000, // processor-specific use
};

enum 
{
	SHL$DYNAMIC 	= SHT$DYNAMIC,
	SHL$HASH 	= SHT$HASH,
	SHL$REL 	= SHT$REL,
	SHL$RELA 	= SHT$RELA,
	SHL$SYMTAB 	= SHT$SYMTAB,
	SHL$DYNSYM 	= SHT$DYNSYM
};

enum 
{
	SHI$REL 	= SHT$REL,
	SHI$RELA 	= SHT$RELA,
	SHI$SYMTAB 	= SHT$SYMTAB,
	SHI$DYNSYM 	= SHT$DYNSYM
};

typedef struct 
{
	elf64_byte elf$ident[EI$NIDENT];
	elf64_half elf$type;
	elf64_half elf$machine;
	elf64_word elf$version;
	elf64_addr elf$entry;
	elf64_off  elf$phoff;
	elf64_off  elf$shoff;
	elf64_word elf$flags;
	elf64_half elf$ehsize;
	elf64_half elf$phentsize;
	elf64_half elf$phnum;
	elf64_half elf$shentsize;
	elf64_half elf$shnum;
	elf64_half elf$shstrndx;
} elf$hdr;

typedef struct {
	elf64_word  sh$name;
	elf64_word  sh$type;
	elf64_xword sh$flags;
	elf64_addr  sh$addr;
	elf64_off   sh$offset;
	elf64_xword sh$size;
	elf64_word  sh$link;
	elf64_word  sh$info;
	elf64_xword sh$addralign;
	elf64_xword sh$entsize;
} elf$shdr;


void elf$hdr_print(elf$hdr*);
void elf$shdr_print(elf$shdr*, u8*);

#endif
