#include "def.h"

#include <stdio.h>

void dump$blob(u8 *buf, u64 len)
{
	for (u64 i = 0; i < len; ++i) {
		printf("%02X", buf[i]); 

		if ((i + 1) % 16 == 0) {
			printf(" | ");

			for (u64 j = i - 15; j <= i; ++j) {
				if (buf[j] >= 33 && buf[j] <= 126)
					printf("%c", buf[j]); 
				else
					printf("."); 
			}

			printf("\n");
		}
		else if ((i + 1) % 8 == 0) {
			printf(" ");
		}
	}

	if (len % 16 == 0) {
		printf("\n");
		return;
	}

	u64 l;
	l = (len / 16) * 16;

	u64 s;
	s = 16 - (len - l);
	for (u64 i = 0; i < s; ++i) {
		printf("  ");
		if ((i + 1) % 8 == 0)
			printf(" ");
	}
	
	printf(" | ");
	
	for (u64 i = l; i < len; ++i) {
		if (buf[i] >= 33 && buf[i] <= 126)
			printf("%c", buf[i]); 
		else
			printf("."); 
	}

	printf("\n");
}
