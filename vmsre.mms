INCLUDES= -
def.h -
msg.h

SOURCES= -
file.c -
main.c -
elf.c -
vec.c -
util.c -
vmsre.cld -
vmsre.msg

.IF DEBUG                                                              
COMPILEFLAGS = /DEBUG /NOOP /LIST=$(MMS$TARGET_NAME) /OBJECT=$(MMS$TARGET)
LINKFLAGS = /DEBUG /MAP=$(MMS$TARGET_NAME) /EXECUTABLE=$(MMS$TARGET)     
.ELSE                                                                  
COMPILEFLAGS = /OBJECT=$(MMS$TARGET)                                   
LINKFLAGS = /EXECUTABLE=$(MMS$TARGET) /NOTRACE
.ENDIF  

.SILENT
OUT_DIR=.BIN
OBJ_DIR=.OBJ

.SUFFIXES
.SUFFIXES .OBJ .C .CLD .MSG
.C.OBJ                                                       
    PIPE CREATE /DIRECTORY $(DIR $(MMS$TARGET)) | COPY SYS$INPUT nl:
    $(CC) $(COMPILEFLAGS) $(MMS$SOURCE)                      
                                                             
.CLD.OBJ                                                     
    PIPE CREATE /DIRECTORY $(DIR $(MMS$TARGET)) | COPY SYS$INPUT nl:
    SET COMMAND /OBJECT=$(MMS$TARGET) $(MMS$SOURCE)           
                                                             
.DEFAULT
	! nop

[$(OUT_DIR)]RE.EXE : -
[$(OBJ_DIR)]vmsre.obj -
[$(OBJ_DIR)]msg.obj -
[$(OBJ_DIR)]file.obj -
[$(OBJ_DIR)]main.obj -
[$(OBJ_DIR)]elf.obj -
[$(OBJ_DIR)]vec.obj -
[$(OBJ_DIR)]util.obj
	LINK $(LINKFLAGS) $(MMS$SOURCE_LIST)

[$(OBJ_DIR)]msg.obj : vmsre.msg
	MESSAGE /SDL /OBJECT=$(MMS$TARGET) $(MMS$SOURCE)
	SDL /LANGUAGE=CC=msg.h vmsre.sdl

vmsre.sdl : vmsre.msg
	MESSAGE /SDL /OBJECT=[$(OBJ_DIR)]msg.obj $(MMS$SOURCE)

msg.h : vmsre.sdl
	SDL /LANGUAGE=CC=$(MMS$TARGET) $(MMS$SOURCE)

[$(OBJ_DIR)]vmsre.obj : vmsre.cld
[$(OBJ_DIR)]file.obj : file.c $(INCLUDES)
[$(OBJ_DIR)]main.obj : main.c $(INCLUDES)
[$(OBJ_DIR)]elf.obj : elf.c $(INCLUDES)
[$(OBJ_DIR)]vec.obj : vec.c $(INCLUDES)
[$(OBJ_DIR)]util.obj : util.c $(INCLUDES)

CLEAN : 
	DELETE /TREE [$(OBJ_DIR)]*.*;*
	DELETE /TREE [$(OUT_DIR)]*.*;*
